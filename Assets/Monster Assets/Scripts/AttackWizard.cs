﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackWizard : MonoBehaviour {

	private GameObject player = null;

	// Use this for initialization
	void Start () {

		player = GameObject.Find ("FirstPersonCharacter");
		InvokeRepeating ("AttackIfPossible", 0.0f, 3.0f);

	}

	void AttackIfPossible() {

		if (GetComponent<AIWizard> ().currentState == AIWizard.WizardState.ATTACK) {

			player.GetComponent<Health> ().DecreaseHealth (10);

		}
	}
}
