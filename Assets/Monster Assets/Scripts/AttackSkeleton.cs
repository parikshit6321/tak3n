﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackSkeleton : MonoBehaviour {

	private GameObject player = null;

	// Use this for initialization
	void Start () {

		player = GameObject.Find ("FirstPersonCharacter");
		InvokeRepeating ("AttackIfPossible", 0.0f, 3.0f);

	}

	void AttackIfPossible() {

		if (GetComponent<AISkeleton> ().currentState == AISkeleton.SkeletonState.ATTACK) {

			player.GetComponent<Health> ().DecreaseHealth (10);

		}
	}
}
