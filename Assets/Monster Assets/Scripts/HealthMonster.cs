﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthMonster : MonoBehaviour {

	private int healthVal = 30;

	public void DecreaseHealthEnemy()
	{
		healthVal -= 10;

		if (healthVal <= 0) {
			this.gameObject.GetComponent<AIMonster> ().currentState = AIMonster.MonsterState.DEATH;
			this.gameObject.GetComponent<Animator> ().Play ("Death");
			this.gameObject.GetComponent<BoxCollider> ().enabled = false;
			Destroy (this.gameObject, 5.0f);
		} else {

			this.gameObject.GetComponent<Animator> ().Play ("Get Hit");

			Invoke ("SwitchAnimationBack", 0.57f);
		}
	}

	private void SwitchAnimationBack() {

		if (this.gameObject.GetComponent<AIMonster> ().currentState == AIMonster.MonsterState.ATTACK) {

			this.gameObject.GetComponent<Animator> ().Play ("Attack");

		} else {

			this.gameObject.GetComponent<Animator> ().Play ("Walk");

		}

	}
}
