﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthWizard : MonoBehaviour {

	private  int healthVal = 20;

	public void DecreaseHealthEnemy()
	{
		healthVal -= 10;

		if (healthVal <= 0) {
			this.gameObject.GetComponent<AIWizard> ().currentState = AIWizard.WizardState.DEATH;
			this.gameObject.GetComponent<Animator> ().Play ("Death");
			this.gameObject.GetComponent<BoxCollider> ().enabled = false;
			Destroy (this.gameObject, 5.0f);
		} else {

			this.gameObject.GetComponent<Animator> ().Play ("Get Hit");

			Invoke ("SwitchAnimationBack", 1.0f);
		}	
	}

	private void SwitchAnimationBack() {

		if (this.gameObject.GetComponent<AIWizard> ().currentState == AIWizard.WizardState.ATTACK) {

			this.gameObject.GetComponent<Animator> ().Play ("Attack");

		} else {

			this.gameObject.GetComponent<Animator> ().Play ("Walk");

		}

	}
}
