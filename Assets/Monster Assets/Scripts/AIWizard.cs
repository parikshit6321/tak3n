﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIWizard : MonoBehaviour {

	public enum WizardState {

		RESURRECTION,
		WALK,
		ATTACK,
		DEATH

	};

	public WizardState currentState = WizardState.RESURRECTION;
	private Vector3 playerPosition = Vector3.zero;

	// Use this for initialization
	public void Spawn () {

		this.gameObject.SetActive (true);

		GetComponent<Animator> ().Play ("Resurrection");
		currentState = WizardState.RESURRECTION;

		InvokeRepeating ("ChangeState", 3.0f, 0.5f);

	}

	private void ChangeState() {

		playerPosition = GameObject.Find ("FirstPersonCharacter").GetComponent<Transform> ().position;
		float distance = Vector3.Distance (playerPosition, transform.position);

		if (currentState != WizardState.DEATH) {

			if (distance < 20.0f) {
				GetComponent<Animator> ().Play ("Attack");
				currentState = WizardState.ATTACK;
			} else {
				GetComponent<Animator> ().Play ("Walk");
				currentState = WizardState.WALK;
			}

		}

	}
}
