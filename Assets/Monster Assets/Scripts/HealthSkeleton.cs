﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthSkeleton : MonoBehaviour {

	private int healthVal = 10;

	public void DecreaseHealthEnemy()
	{
		healthVal -= 10;

		if (healthVal <= 0) {
			this.gameObject.GetComponent<AISkeleton> ().currentState = AISkeleton.SkeletonState.DEATH;
			this.gameObject.GetComponent<Animator> ().Play ("Death");
			this.gameObject.GetComponent<BoxCollider> ().enabled = false;
			Destroy (this.gameObject, 5.0f);
		}	
	}
}