﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementWizard : MonoBehaviour {

	private float displacementSpeed = 1.0f;
	private float rotationSpeed = 2.0f;

	// Update is called once per frame
	void Update () {

		if (GetComponent<AIWizard> ().currentState == AIWizard.WizardState.WALK) {

			Vector3 playerPosition = GameObject.Find ("FirstPersonCharacter").GetComponent<Transform> ().position;
			Vector3 targetDirection = playerPosition - transform.position;

			float rotationStep = rotationSpeed * Time.deltaTime;
			Vector3 rotationDirection = Vector3.RotateTowards (transform.forward, targetDirection, rotationStep, 0.0f);
			transform.rotation = Quaternion.LookRotation (rotationDirection);

			playerPosition.y = transform.position.y;

			float displacementStep = displacementSpeed * Time.deltaTime;
			transform.position = Vector3.MoveTowards (transform.position, playerPosition, displacementStep);

		}
	}
}
