﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISkeleton : MonoBehaviour {

	public enum SkeletonState {

		RESURRECTION,
		WALK,
		ATTACK,
		DEATH

	};

	public SkeletonState currentState = SkeletonState.RESURRECTION;
	private Vector3 playerPosition = Vector3.zero;

	// Use this for initialization
	public void Spawn () {

		this.gameObject.SetActive (true);

		GetComponent<Animator> ().Play ("Resurrection");
		currentState = SkeletonState.RESURRECTION;

		InvokeRepeating ("ChangeState", 5.8f, 0.5f);

	}

	private void ChangeState() {

		playerPosition = GameObject.Find ("FirstPersonCharacter").GetComponent<Transform> ().position;
		float distance = Vector3.Distance (playerPosition, transform.position);

		if (currentState != SkeletonState.DEATH) {

			if (distance < 4.0f) {
				GetComponent<Animator> ().Play ("Attack");
				currentState = SkeletonState.ATTACK;
			} else {
				GetComponent<Animator> ().Play ("Walk");
				currentState = SkeletonState.WALK;
			}

		}

	}

}
