﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMonster : MonoBehaviour {

	public enum MonsterState {

		RESURRECTION,
		WALK,
		ATTACK,
		DEATH

	};

	public MonsterState currentState = MonsterState.RESURRECTION;
	private Vector3 playerPosition = Vector3.zero;

	// Use this for initialization
	public void Spawn () {

		this.gameObject.SetActive (true);

		GetComponent<Animator> ().Play ("Resurrection");
		currentState = MonsterState.RESURRECTION;

		InvokeRepeating ("ChangeState", 1.74f, 0.5f);

	}

	private void ChangeState() {

		playerPosition = GameObject.Find ("FirstPersonCharacter").GetComponent<Transform> ().position;
		float distance = Vector3.Distance (playerPosition, transform.position);

		if (currentState != MonsterState.DEATH) {

			if (distance < 7.0f) {
				GetComponent<Animator> ().Play ("Attack");
				currentState = MonsterState.ATTACK;
			} else {
				GetComponent<Animator> ().Play ("Walk");
				currentState = MonsterState.WALK;
			}

		}

	}
}
