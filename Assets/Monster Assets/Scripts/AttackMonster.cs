﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackMonster : MonoBehaviour {

	private GameObject player = null;

	// Use this for initialization
	void Start () {

		player = GameObject.Find ("FirstPersonCharacter");
		InvokeRepeating ("AttackIfPossible", 0.0f, 3.0f);

	}

	void AttackIfPossible() {

		if (GetComponent<AIMonster> ().currentState == AIMonster.MonsterState.ATTACK) {

			player.GetComponent<Health> ().DecreaseHealth (10);

		}
	}
}
