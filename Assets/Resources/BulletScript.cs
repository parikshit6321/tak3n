﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {

	public float speed = 10.0f;
	public float activeTime = 10.0f;
	private Vector3 origin = Vector3.zero;
	private Vector3 direction = Vector3.zero;
	private Quaternion playerRotation = Quaternion.identity;

	// Use this for initialization
	void Start () {

		playerRotation = GameObject.Find ("FPSController").transform.rotation;
		playerRotation *= Quaternion.Euler (0.0f, 90.0f, 0.0f);
		this.transform.rotation = playerRotation;

		origin = GameObject.Find ("FirstPersonCharacter").GetComponent<Camera> ().transform.position;
		this.transform.position = origin;
		direction = GameObject.Find ("FirstPersonCharacter").GetComponent<Camera>().transform.forward;

		Destroy (this.gameObject, activeTime);
	}
	
	// Update is called once per frame
	void Update () {

		this.transform.position += (direction * (speed * Time.deltaTime));

	}
}
