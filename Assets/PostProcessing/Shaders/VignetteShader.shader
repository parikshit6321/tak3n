﻿Shader "Hidden/VignetteShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}


	CGINCLUDE

	#include "UnityCG.cginc"
	uniform sampler2D _MainTex;
	uniform sampler2D vignetteTexture;
	uniform half4 vignetteColor;
	uniform half globalVignetteIntensity;
	struct appdata
	{
		float4 vertex : POSITION;
		float2 uv : TEXCOORD0;
	};

	struct v2f
	{
		float2 uv : TEXCOORD0;
		float4 vertex : SV_POSITION;
	};


	v2f vert_vignette (appdata v)
	{
		v2f o;
		o.vertex = UnityObjectToClipPos(v.vertex);
		o.uv = v.uv;
		return o;
	}
			


	half4 frag_vignette (v2f i) : SV_Target
	{
		half4 original = tex2D(_MainTex, i.uv);
		half vignetteIntensity = tex2D(vignetteTexture, i.uv).r;
		// just invert the colors
		half4 col = original;
	 	col.rgb = lerp(original.rgb, vignetteColor.rgb, vignetteIntensity * globalVignetteIntensity) ;
		return col;
	}


	ENDCG
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM

			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma target 3.0
			#pragma vertex vert_vignette
			#pragma fragment frag_vignette


			ENDCG
		}
	}
}
