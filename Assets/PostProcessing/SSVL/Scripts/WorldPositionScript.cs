﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class WorldPositionScript : MonoBehaviour {

	public RenderTexture ssvlWorldPositionMap = null;
	public Shader worldPositionShader = null;
	public float worldVolumeBoundary = 50.0f;
	public Vector2Int shadowMapResolution = Vector2Int.zero;

	private GameObject[] ssvlDisable = null;

	// Use this for initialization
	public void Initialize () {

		ssvlDisable = GameObject.FindGameObjectsWithTag ("SSVLDisable");
			
		ssvlWorldPositionMap = new RenderTexture (shadowMapResolution.x, shadowMapResolution.y, 0, RenderTextureFormat.ARGB32);
		GetComponent<Camera> ().targetTexture = ssvlWorldPositionMap;
	
	}
	
	// Update is called once per frame
	public void RenderWorldPositionTexture () {

		for (int i = 0; i < ssvlDisable.Length; ++i)
			ssvlDisable[i].SetActive(false);

		Shader.SetGlobalFloat ("worldVolumeBoundary", worldVolumeBoundary);
		GetComponent<Camera> ().RenderWithShader(worldPositionShader, null);

		for (int i = 0; i < ssvlDisable.Length; ++i)
			ssvlDisable[i].SetActive(true);
	}

	void OnDestroy() {

		ssvlWorldPositionMap.Release ();

	}
}
