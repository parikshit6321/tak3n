﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SSVL : MonoBehaviour {

	public Shader shader = null;
	public float threshold = 0.1f;
	public int numOfSamples = 32;
	public Color fogColor = Color.gray;
	public float lightShaftsStrength = 0.3f;
	public int downsample = 1;
	public int blurIterations = 1;
	public Texture weaponMask = null;
	public float depthCutOff = 1.0f;

	private bool lightShaftsOn = true;
	private Vector3 lightDirection = Vector3.zero;

	private Material material = null;

	private Camera[] cameras = null;
	private Camera ssvlCamera = null;

	private Light[] lights = null;
	private Light mainDirectionalLight = null;

	// Use this for initialization
	void Start () {

		lightShaftsOn = GameObject.Find ("GlobalSettings").GetComponent<GlobalSettings> ().lightShaftsSettings;

		if (shader != null) {

			material = new Material (shader);

		}

		GetComponent<Camera> ().depthTextureMode = DepthTextureMode.Depth;

		cameras = Resources.FindObjectsOfTypeAll<Camera> ();

		for (int i = 0; i < cameras.Length; ++i) {

			if (cameras [i].name.CompareTo ("SSVLCamera") == 0) {

				ssvlCamera = cameras [i];
				ssvlCamera.GetComponent<WorldPositionScript> ().Initialize ();
				ssvlCamera.GetComponent<WorldPositionScript> ().RenderWorldPositionTexture ();
				break;

			}

		}

		lights = Resources.FindObjectsOfTypeAll<Light> ();

		for (int i = 0; i < lights.Length; ++i) {

			if (lights [i].name.CompareTo ("DirectionalLight") == 0) {

				lightDirection = lights [i].transform.forward;

			}

		}
	}
	
	// Update is called once per frame
	void OnRenderImage (RenderTexture source, RenderTexture destination) {

		if (lightShaftsOn) {

			RenderTexture temp = RenderTexture.GetTemporary (source.width / downsample, source.height / downsample, 0, RenderTextureFormat.ARGB32);
			RenderTexture lightShafts = RenderTexture.GetTemporary (source.width / downsample, source.height / downsample, 0, RenderTextureFormat.ARGB32);

			RenderTexture HBlurredTex = RenderTexture.GetTemporary (source.width / downsample, source.height / downsample, 0, RenderTextureFormat.ARGB32);
			RenderTexture HVBlurredTex = RenderTexture.GetTemporary (source.width / downsample, source.height / downsample, 0, RenderTextureFormat.ARGB32);

			Matrix4x4 ssvlTransformationMatrix = ssvlCamera.projectionMatrix * ssvlCamera.worldToCameraMatrix;

			material.SetTexture ("ssvlWorldPositionMap", ssvlCamera.GetComponent<WorldPositionScript>().ssvlWorldPositionMap);
			material.SetTexture ("weaponMask", weaponMask);
			material.SetMatrix( "InverseViewMatrix", GetComponent<Camera>().cameraToWorldMatrix);
			material.SetMatrix( "InverseProjectionMatrix", GetComponent<Camera>().projectionMatrix.inverse);
			material.SetMatrix ("ssvlTransformationMatrix", ssvlTransformationMatrix);
			material.SetVector ("mainCameraPosition", GetComponent<Camera>().transform.position);
			material.SetVector ("lightDirection", lightDirection);
			material.SetColor ("fogColor", fogColor);
			material.SetFloat ("threshold", threshold);
			material.SetFloat ("numOfSamples", (float)numOfSamples);
			material.SetFloat ("worldVolumeBoundary", ssvlCamera.GetComponent<WorldPositionScript> ().worldVolumeBoundary);
			material.SetFloat ("lightShaftsStrength", lightShaftsStrength);
			material.SetFloat ("depthCutOff", depthCutOff);

			Graphics.Blit (source, temp);

			Graphics.Blit (temp, lightShafts, material, 0);

			// Blur the light shafts texture
			for (int i = 0; i < blurIterations; ++i) {

				// Horizontal Blur Pass
				Graphics.Blit (lightShafts, HBlurredTex, material, 1);

				// Vertical Blur Pass
				Graphics.Blit (HBlurredTex, HVBlurredTex, material, 2);

				Graphics.Blit (HVBlurredTex, lightShafts);

			}

			material.SetTexture ("lightShafts", lightShafts);

			Graphics.Blit (source, destination, material, 3);

			RenderTexture.ReleaseTemporary (temp);
			RenderTexture.ReleaseTemporary (lightShafts);

			RenderTexture.ReleaseTemporary (HBlurredTex);
			RenderTexture.ReleaseTemporary (HVBlurredTex);

		} else {
		
			Graphics.Blit (source, destination);

		}

	}
}
