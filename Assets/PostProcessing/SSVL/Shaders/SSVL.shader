﻿Shader "Hidden/SSVL"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		CGINCLUDE

		#include "UnityCG.cginc"

		uniform sampler2D 	_MainTex;
		uniform sampler2D	_CameraDepthTexture;
		uniform sampler2D	ssvlWorldPositionMap;
		uniform sampler2D	lightShafts;
		uniform sampler2D	weaponMask;

		uniform half4x4		InverseProjectionMatrix;
		uniform half4x4		InverseViewMatrix;
		uniform half4x4		ssvlTransformationMatrix;

		uniform half4		_MainTex_TexelSize;

		uniform float3		mainCameraPosition;
		uniform half3		fogColor;
		uniform half3		lightDirection;

		uniform half		threshold;
		uniform half		numOfSamples;
		uniform half		worldVolumeBoundary;
		uniform half		lightShaftsStrength;
		uniform half 		depthCutOff;

		struct appdata
		{
			float4 vertex : POSITION;
			float2 uv : TEXCOORD0;

		};

		struct v2f
		{
			float2 uv : TEXCOORD0;
			float4 vertex : SV_POSITION;
			float4 cameraRay : TEXCOORD1;
		};

		struct v2f_blur
		{
			float2 uv : TEXCOORD0;
			float4 vertex : SV_POSITION;
			half2 offset1 : TEXCOORD1;
			half2 offset2 : TEXCOORD2;
			half2 offset3 : TEXCOORD3;
		};

		v2f_blur vert_horizontal_blur(appdata v)
		{
			half unitX = _MainTex_TexelSize.x;

			v2f_blur o;

			o.vertex = UnityObjectToClipPos(v.vertex);
			
			o.uv = v.uv;

			o.offset1 = half2(o.uv.x - unitX, o.uv.y);
			o.offset2 = half2(o.uv.x, o.uv.y);
			o.offset3 = half2(o.uv.x + unitX, o.uv.y);
			
			return o;
		}

		v2f_blur vert_vertical_blur(appdata v)
		{
			half unitY = _MainTex_TexelSize.y;

			v2f_blur o;

			o.vertex = UnityObjectToClipPos(v.vertex);
			
			o.uv = v.uv;

			o.offset1 = half2(o.uv.x, o.uv.y - unitY);
			o.offset2 = half2(o.uv.x, o.uv.y);
			o.offset3 = half2(o.uv.x, o.uv.y + unitY);

			return o;
		}

		v2f vert (appdata v)
		{
			v2f o;
			o.vertex = UnityObjectToClipPos(v.vertex);
			o.uv = v.uv;

			//transform clip pos to view space
			float4 clipPos = float4( v.uv * 2.0 - 1.0, 1.0, 1.0);
			float4 cameraRay = mul(InverseProjectionMatrix, clipPos);
			o.cameraRay = cameraRay / cameraRay.w;

			return o;
		}

		float3 DecodePosition(float3 input)
		{
			float3 output = input;

			output *= 2.0f;
			output -= float3(1.0f, 1.0f, 1.0f);
			output *= worldVolumeBoundary;

			return output;
		}

		half ComputeVisibility(float3 worldPos)
		{
			half3 shadowCoord = mul(ssvlTransformationMatrix, float4(worldPos,1)).xyz; 

			shadowCoord += half3(1.0f, 1.0f, 1.0f);
			shadowCoord /= 2.0f;

			float3 nearestFragmentPosition = DecodePosition(tex2D(ssvlWorldPositionMap, shadowCoord.xy).xyz);

			float3 worldPosToNearestDirection = nearestFragmentPosition - worldPos;

			half dotOfBoth = dot(worldPosToNearestDirection, lightDirection);

			half isVisible = 1.0f;

			if (dotOfBoth < threshold)
			{
				isVisible = 0.0f;
			}

			return isVisible;
		}

		half4 frag_light_shafts (v2f i) : SV_Target
		{
			half4 col = tex2D(_MainTex, i.uv);

			// read low res depth and reconstruct world position
			float depth = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, i.uv);
			
			//linearise depth		
			float lindepth = Linear01Depth (depth);
			
			//get view and then world positions		
			float4 viewPos = float4(i.cameraRay.xyz * lindepth,1);
			float3 worldPos = mul(InverseViewMatrix, viewPos).xyz;

			float3 rayDirection = normalize(mainCameraPosition - worldPos);
			half rayStep = length(mainCameraPosition - worldPos) / numOfSamples;

			half accumulatedIntensity = 0.0f;

			float3 currentPosition = worldPos;

			for (int i = 0; i < numOfSamples; ++i)
			{
				accumulatedIntensity += ComputeVisibility(currentPosition);

				currentPosition += (rayDirection * rayStep);
			}

			accumulatedIntensity /= numOfSamples;

			half3 finalColor = fogColor * accumulatedIntensity;

			finalColor.rgb *= saturate(depthCutOff - lindepth);

			return half4(finalColor, 1.0f);
		}

		half4 frag_blur(v2f_blur i) : SV_TARGET
		{
			half4 col;
			
			col = tex2D(_MainTex, i.offset1);
			col += tex2D(_MainTex, i.offset2);
			col += tex2D(_MainTex, i.offset3);

			col *= 0.33;

			return col;
		}

		half4 frag_blend(v2f i) : SV_TARGET
		{
			half4 col = tex2D(_MainTex, i.uv);
			half4 fogColor = tex2D(lightShafts, i.uv);

			half mask = tex2D(weaponMask, i.uv);
			lightShaftsStrength *= (1.0f - mask);

			half3 finalColor = lerp(col.rgb, fogColor.rgb, lightShaftsStrength);
			return half4(finalColor, 1.0f);
		}

		ENDCG

		// 0 : Light Shafts Pass
		Pass
		{
			CGPROGRAM
			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma vertex vert
			#pragma fragment frag_light_shafts
			#pragma target 3.0
			ENDCG
		}

		// 1 : Horizontal Blur Pass
		Pass
		{
			CGPROGRAM

			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma vertex vert_horizontal_blur
			#pragma fragment frag_blur
			#pragma target 3.0

			ENDCG
		}

		// 2 : Vertical Blur Pass
		Pass
		{
			CGPROGRAM

			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma vertex vert_vertical_blur
			#pragma fragment frag_blur
			#pragma target 3.0

			ENDCG
		}

		// 3 : Blend Pass
		Pass
		{
			CGPROGRAM

			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma vertex vert
			#pragma fragment frag_blend
			#pragma target 3.0

			ENDCG
		}
	}
}
