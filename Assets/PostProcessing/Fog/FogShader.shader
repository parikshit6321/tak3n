﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Hidden/FogShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}

	CGINCLUDE

	#include "UnityCG.cginc"

	// Input Textures
	uniform sampler2D 	_MainTex;
	uniform sampler2D	_CameraDepthTexture;

	// Input Parameters
	uniform float4 		_MainTex_TexelSize;
	uniform float4		_CameraDepthTexture_TexelSize;
	uniform float		fogDensityDepth;
	uniform float		fogDensityAltitude;
	uniform float4		fogColor;

	struct appdata
	{
		float4 vertex 	: POSITION;
		float2 uv 		: TEXCOORD0;
	};

	struct v2f_fog
	{
		float4 position	: SV_POSITION;
		float2 uv 		: TEXCOORD0;
	};

	// Vertex Shader for fog computation
	v2f_fog vert_fog(appdata i)
	{
		v2f_fog o;
		o.position = UnityObjectToClipPos(i.vertex);
		o.uv = i.uv;

		#if UNITY_UV_STARTS_AT_TOP
		if (_MainTex_TexelSize.y < 0)
			o.uv.y = 1 - o.uv.y;
		#endif

		return o;
	}

	float4 frag_fog(v2f_fog i) : SV_Target
	{
		float4 texColor = tex2D(_MainTex, i.uv);
		float depth = Linear01Depth(tex2D(_CameraDepthTexture, i.uv));

		float finalFogDepth = 0.0f;

		float distanceMulDensity = depth * fogDensityDepth;
		float exponentialDepth = exp(distanceMulDensity) * exp(distanceMulDensity);

		finalFogDepth = clamp(exponentialDepth - 1.0f, 0.0f, 1.0f);

		float finalFog = finalFogDepth;

		float4 finalColor = lerp(texColor, fogColor, finalFog);

		return finalColor;
	}

	ENDCG

	SubShader
	{

		// No culling or depth test
		Cull Off ZWrite Off ZTest Always

		// 0 : Fog Pass
		Pass
		{

			CGPROGRAM

			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma vertex vert_fog
			#pragma fragment frag_fog
			#pragma target 3.0

			ENDCG

		}

	}
}
