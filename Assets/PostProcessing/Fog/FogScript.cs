﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class FogScript : MonoBehaviour {

	public Shader fogShader = null;

	[Range (0.0f, 1000.0f)]
	public float fogDensityDepth = 0.1f;

	[Range (0.0f, 1000.0f)]
	public float fogDensityAltitude = 0.1f;

	public Color fogColor = Color.gray;

	private Material fogMaterial = null;

	// Use this for initialization
	void Start () {

		if (fogShader != null)
			fogMaterial = new Material (fogShader);

		GetComponent<Camera>().depthTextureMode = DepthTextureMode.Depth;

	}
	
	// Called after the rendering the scene with lighting
	void OnRenderImage (RenderTexture source, RenderTexture destination) {

		fogMaterial.SetFloat ("fogDensityDepth", fogDensityDepth);
		fogMaterial.SetFloat ("fogDensityAltitude", fogDensityAltitude);
		fogMaterial.SetColor ("fogColor", fogColor);
		Graphics.Blit (source, destination, fogMaterial);

	}
}
