﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class AmbientOcclusionSettings : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
		GlobalSettings.GraphicsSettings ambientOcclusionSettings = GameObject.Find ("GlobalSettings").GetComponent<GlobalSettings> ().ambientOcclusionSettings;

		AmbientOcclusionModel.Settings aoSettings = GetComponent<PostProcessingBehaviour> ().profile.ambientOcclusion.settings;

		if (ambientOcclusionSettings == GlobalSettings.GraphicsSettings.OFF) {

			GetComponent<PostProcessingBehaviour> ().profile.ambientOcclusion.enabled = false;

		} else if (ambientOcclusionSettings == GlobalSettings.GraphicsSettings.LOW) {

			aoSettings.sampleCount = AmbientOcclusionModel.SampleCount.Low;

		} else if (ambientOcclusionSettings == GlobalSettings.GraphicsSettings.MEDIUM) {

			aoSettings.sampleCount = AmbientOcclusionModel.SampleCount.Medium;

		} else {

			aoSettings.sampleCount = AmbientOcclusionModel.SampleCount.High;

		}

		GetComponent<PostProcessingBehaviour> ().profile.ambientOcclusion.settings = aoSettings;
	}

}
