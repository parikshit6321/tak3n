﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class GodRays : MonoBehaviour {

	public Shader shader = null;
    
	public Texture weaponMask = null;

    public float weight = 1.0f;
    public float decay = 1.0f;
    public float exposure = 1.0f;
    public float numOfSamples = 32.0f;
    
    public int downSample = 4;

	private bool godRaysOn = true;

    private Vector2 sunScreenPosition = new Vector2(0.0f, 0.5f);

	private Material material = null;

	private Camera mainCameraReference = null;
	private Light mainLightReference = null;

	private bool bFacingSun = true;

	// Use this for initialization
	void Start () {
	
		//godRaysOn = GameObject.Find ("GlobalSettings").GetComponent<GlobalSettings> ().godRaysSettings;

        if(shader != null)
        {
            material = new Material(shader);
        }

		mainCameraReference = GetComponent<Camera> ();
		mainLightReference = FindObjectOfType<Light> ();

		mainCameraReference.depthTextureMode = DepthTextureMode.Depth;

	}

	void SetSunScreenPosition() {

		Vector3 cameraToLightDir = mainLightReference.transform.forward.normalized * -1.0f;
		Vector3 cameraForwardDir = mainCameraReference.transform.forward.normalized;

		Vector3 cameraToLightScreen = mainCameraReference.WorldToScreenPoint(cameraToLightDir);
		Vector3 cameraForwardScreen = mainCameraReference.WorldToScreenPoint(cameraForwardDir);


		cameraToLightDir.y = 0.0f;
		cameraToLightDir.Normalize ();

		cameraForwardDir.y = 0.0f;
		cameraForwardDir.Normalize ();

		float dotProduct = Vector3.Dot (cameraToLightDir, cameraForwardDir);

		if (dotProduct > 0.0f) {

			bFacingSun = true;

			// Point is on the left side of the screen.
			if (cameraForwardScreen.x < cameraToLightScreen.x) {
				sunScreenPosition.x = (dotProduct / 2.0f);
			} 
			// Point is on the right side of the screen.
			else if (cameraForwardScreen.x > cameraToLightScreen.x) {
				sunScreenPosition.x = 0.5f + ((1.0f - dotProduct) / 2.0f);
			} 
			// Point is in the middle of the screen.
			else {
				sunScreenPosition.x = 0.5f;
			}

			sunScreenPosition.y = 1.0f;


		} else {

			// Make sure no rays come through the trees when not facing the sun.
			bFacingSun = false;
		
		}
			
	}

	// This is called once per frame
	void OnRenderImage (RenderTexture source, RenderTexture destination) {

		if (godRaysOn) {

			SetSunScreenPosition();

			if (bFacingSun) {

				material.SetFloat("Weight", weight);
				material.SetFloat("Decay", decay);
				material.SetFloat("Exposure", exposure);
				material.SetFloat("NumOfSamples", numOfSamples);
				material.SetVector("SunScreenPos", sunScreenPosition);
				material.SetTexture ("WeaponMask", weaponMask);

				//RenderTexture maskedTexture = RenderTexture.GetTemporary(source.width / downSample, source.height / downSample);
				RenderTexture godRaysTexture = RenderTexture.GetTemporary(source.width / downSample, source.height / downSample);
				RenderTexture HBlurredTexture = RenderTexture.GetTemporary(source.width / downSample, source.height / downSample);
				RenderTexture HVBlurredTexture = RenderTexture.GetTemporary(source.width / downSample, source.height / downSample);

				// Perform god rays pass
				Graphics.Blit(source, godRaysTexture, material, 0);

				// Blur the obtained god rays texture
				// Horizonatal Blur Pass
				Graphics.Blit(godRaysTexture, HBlurredTexture, material, 1);
				// Vertical Blur Pass
				Graphics.Blit(HBlurredTexture, HVBlurredTexture, material, 2);

				// Blend the two textures together
				material.SetTexture("GodTexture", HVBlurredTexture);

				Graphics.Blit(source, destination, material, 3);

				//RenderTexture.ReleaseTemporary(maskedTexture);
				RenderTexture.ReleaseTemporary(godRaysTexture);
				RenderTexture.ReleaseTemporary(HBlurredTexture);
				RenderTexture.ReleaseTemporary(HVBlurredTexture);


			} 
			else {

				Graphics.Blit (source, destination);

			}

		} else {

			Graphics.Blit (source, destination);

		}

    }
}
