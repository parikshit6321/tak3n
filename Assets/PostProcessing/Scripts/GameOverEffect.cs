﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverEffect : MonoBehaviour {

	public Shader gameOverShader = null;
	public Texture gameOverTexture = null;

	public float fadeInFactor = 0.02f;

	private Material gameOverMaterial = null;
	private float lerpValue = 0.0f;

	// Use this for initialization
	void Start () {

		if (gameOverShader != null) {

			gameOverMaterial = new Material (gameOverShader);
			gameOverMaterial.SetTexture ("gameOverTexture", gameOverTexture);

		}

	}

	private void IncreaseLerpValue() {

		if (lerpValue < 0.6f) {

			lerpValue += (fadeInFactor * Time.deltaTime);

		}

	}

	// Update is called once per frame
	void OnRenderImage (RenderTexture source, RenderTexture destination) {

		if (GameObject.Find ("FirstPersonCharacter").GetComponent<Health> ().isGameOver) {
		
			gameOverMaterial.SetFloat ("lerpValue", lerpValue);

			IncreaseLerpValue ();

			Graphics.Blit (source, destination, gameOverMaterial);

		} else {

			Graphics.Blit (source, destination);

		}
	}
}
