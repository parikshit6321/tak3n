﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class VignetteScript : MonoBehaviour {

	public Shader vignetteShader = null;
	public Texture vignetteTexture = null;
	public Color vignetteColor = Color.red;
	public float fadeOutFactor = 0.02f;

	private float globalVignetteIntensity = 0.0f;
	private Material vignetteMaterial = null;
	private bool vignetteActivated = false;
	private float totalDeltaTime = 0.0f;

	// Use this for initialization
	void Start () {
		if (vignetteShader != null) 
		{
			vignetteMaterial = new Material (vignetteShader);
			vignetteMaterial.SetTexture ("vignetteTexture", vignetteTexture);

		}
	}

	public void ActivateVignetteEffect () {

		vignetteActivated = true;

	}

	void StartVignette() {
		
		globalVignetteIntensity = 1.0f;
	}

	void FadeOut() {
		
		globalVignetteIntensity -= (fadeOutFactor * Time.deltaTime);
		Mathf.Clamp01 (globalVignetteIntensity);
	
	}

	// Update is called once per frame
	void OnRenderImage (RenderTexture source, RenderTexture destination) {
		
		if (vignetteActivated) {
		
			totalDeltaTime += Time.deltaTime;
			if (totalDeltaTime < 0.1f) {
				StartVignette ();
			} else if (totalDeltaTime > 1.0f && totalDeltaTime < 1.5f) {
				FadeOut ();
			} else if (totalDeltaTime > 1.5f) {
				vignetteActivated = false;
				totalDeltaTime = 0.0f;
				globalVignetteIntensity = 0.0f;
			}

			vignetteMaterial.SetFloat ("globalVignetteIntensity", globalVignetteIntensity);
			vignetteMaterial.SetColor ("vignetteColor", vignetteColor);
			Graphics.Blit (source, destination, vignetteMaterial);

		} else {

			Graphics.Blit (source, destination);

		}
	}
}
