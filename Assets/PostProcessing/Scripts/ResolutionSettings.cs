﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResolutionSettings : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
		if (GameObject.Find ("GlobalSettings").GetComponent<GlobalSettings> ().resolutionSettings == GlobalSettings.ResolutionSettings.P1080) {

			Screen.SetResolution (1920, 1080, true);
		
		} else if (GameObject.Find ("GlobalSettings").GetComponent<GlobalSettings> ().resolutionSettings == GlobalSettings.ResolutionSettings.P720) {

			Screen.SetResolution (1280, 720, true);

		} else {

			Screen.SetResolution(1136, 640, true);

		}

	}

}
