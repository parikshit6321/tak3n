﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour {

	public int health =100;
	public bool isGameOver = false;

	public Sprite gameOverScreen = null;

	private bool vibrationEnabled = false;

	void Start() 
	{

		vibrationEnabled = GameObject.Find ("GlobalSettings").GetComponent<GlobalSettings>().isVibrationEnabled;

	}

	public void IncreaseHealth(int amt)
	{
		health += amt;
		if (health >= 100)
			health = 100;
	}

	public void DecreaseHealth(int amt)
	{
		GetComponent<VignetteScript> ().ActivateVignetteEffect ();

		if (vibrationEnabled)
			Handheld.Vibrate ();

		health -= amt;
		if (health <= 0) {
			health = 0;
			GameOver ();
		}
	}

	private void ReturnToMainMenu() {

		GameObject[] disableOnLoad = GameObject.FindGameObjectsWithTag ("DisableOnLoad");
		for (int i = 0; i < disableOnLoad.Length; ++i)
			disableOnLoad [i].SetActive (true);
		SceneManager.LoadScene ("Menu");

	}

	private void GameOver()
	{
		isGameOver = true;
		Invoke ("ReturnToMainMenu", 7.0f);
		GameObject[] disableOnLoad = GameObject.FindGameObjectsWithTag ("DisableOnLoad");
		for (int i = 0; i < disableOnLoad.Length; ++i)
			disableOnLoad [i].SetActive (false);
	}

}
