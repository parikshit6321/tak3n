﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChanger : MonoBehaviour {

	public Sprite loadingScreen = null;
	public string nextScene = null;

	void OnTriggerEnter(Collider other) {

		if (other.gameObject.name.CompareTo ("FirstPersonCharacter") == 0) {

			GameObject[] disableOnLoad = GameObject.FindGameObjectsWithTag ("DisableOnLoad");
			for (int i = 0; i < disableOnLoad.Length; ++i)
				disableOnLoad [i].SetActive (false);

			GameObject.Find ("BackgroundCanvas/background").GetComponent<UnityEngine.UI.Image> ().enabled = true;

			SceneManager.LoadScene (nextScene);

		}

	}
}
