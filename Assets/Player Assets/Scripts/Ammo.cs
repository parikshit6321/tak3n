﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour {

	public float maximumAmmo = 5.0f;
	public float currentAmmo = 0.0f;
	public float rechargeFactor = 0.1f;

	// Use this for initialization
	void Start () {

		currentAmmo = maximumAmmo;

	}

	public bool IsAmmoAvailable() {

		return ((int)currentAmmo > 0);

	}

	public void UseAmmo() {

		currentAmmo -= 1.0f;

		if (currentAmmo < 0.0f)
			currentAmmo = 0.0f;

	}

	private void RechargeAmmoIfDepleted() {

		if ((int)currentAmmo < (int)maximumAmmo) {

			currentAmmo += (rechargeFactor * Time.deltaTime);

		}

	}

	// Update is called once per frame
	void Update () {

		RechargeAmmoIfDepleted ();

	}
}
