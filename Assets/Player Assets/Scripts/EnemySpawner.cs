﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {
	public List<GameObject> spawner  = new List<GameObject> ();
	public GameObject player = null; 
	public float threshold_distance = 10.0f;
	private GameObject[] tempArraySkeleton = null;
	private GameObject[] tempArrayMonster = null;
	private GameObject[] tempArrayWizard = null;

	// Use this for initialization
	void Start () {

		// Handle skeletons first
		tempArraySkeleton = GameObject.FindGameObjectsWithTag ("Skeleton");

		for (int i = 0; i < tempArraySkeleton.Length; ++i) {
			spawner.Add (tempArraySkeleton [i]);
		}

		tempArraySkeleton = null;

		// Now handle monsters
		tempArrayMonster = GameObject.FindGameObjectsWithTag("Monster");

		for (int i = 0; i < tempArrayMonster.Length; ++i) {
			spawner.Add (tempArrayMonster [i]);
		}

		tempArrayMonster = null;

		// Now handle wizards
		tempArrayWizard = GameObject.FindGameObjectsWithTag("Wizard");

		for (int i = 0; i < tempArrayWizard.Length; ++i) {
			spawner.Add (tempArrayWizard [i]);
		}

		tempArrayWizard = null;

		// Make all of them inactive
		for (int i = 0; i < spawner.Count; i++) {
			spawner [i].SetActive (false);
		}

		player = GameObject.Find ("FirstPersonCharacter");

		InvokeRepeating ("SpawnEnemyIfPossible", 0.0f, 1.0f);
	}

	private void SpawnEnemyAtIndex(int index)
	{
		if (spawner [index].tag == "Skeleton") {
			spawner [index].GetComponent<AISkeleton> ().Spawn ();
		} else if (spawner [index].tag == "Monster") {
			spawner [index].GetComponent<AIMonster> ().Spawn ();
		} else if (spawner [index].tag == "Wizard") {
			spawner [index].GetComponent<AIWizard> ().Spawn ();
		}

		spawner.RemoveAt (index);
	}
		
	private void SpawnEnemyIfPossible()
	{
		for (int i = 0; i < spawner.Count; i++) {
			float distance = Vector3.Distance (player.transform.position, spawner[i].transform.position);
			if (distance <= threshold_distance) {

				SpawnEnemyAtIndex (i);

			}
		}
	}
}
