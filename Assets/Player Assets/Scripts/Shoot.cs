﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour {

	public int damage = 10;
	public float range = 200.0f;

	private GameObject spawnedBullet = null;

	public void ShootWeapon () {

		if (GameObject.Find("ShootButtonHandler").GetComponent<Ammo> ().IsAmmoAvailable ()) {
		
			GameObject.Find ("ShootAudioSource").GetComponent<AudioSource> ().Play ();

			GameObject.Find("ShootButtonHandler").GetComponent<Ammo> ().UseAmmo ();

			Camera fpsCam = GameObject.Find("FirstPersonCharacter").GetComponent<Camera>();
			RaycastHit hit;

			spawnedBullet = (GameObject)Instantiate(Resources.Load("Bullet"));

			if (Physics.Raycast(GameObject.Find ("FirstPersonCharacter").transform.position, GameObject.Find ("FirstPersonCharacter").transform.forward, out hit, range)) {

				if (hit.collider.gameObject.tag == "Monster") {

					hit.collider.gameObject.GetComponent<HealthMonster> ().DecreaseHealthEnemy ();

				} 
				else if (hit.collider.gameObject.tag == "Skeleton") {

					hit.collider.gameObject.GetComponent<HealthSkeleton> ().DecreaseHealthEnemy ();

				} else if (hit.collider.gameObject.tag == "Wizard") {

					hit.collider.gameObject.GetComponent<HealthWizard> ().DecreaseHealthEnemy ();

				}
			}

		}

	}

}
