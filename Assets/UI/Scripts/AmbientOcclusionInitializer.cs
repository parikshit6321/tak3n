﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AmbientOcclusionInitializer : MonoBehaviour {

	// Use this for initialization
	void Start () {

		GetComponent<Slider> ().value = (int)GameObject.Find ("GlobalSettings").GetComponent<GlobalSettings> ().ambientOcclusionSettings;

	}
}
