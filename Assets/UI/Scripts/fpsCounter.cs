﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class fpsCounter : MonoBehaviour {

	private Text fpsString;
	private int fps;

	// Use this for initialization
	void Start () {
		fps = 0;
		fpsString = GetComponent<UnityEngine.UI.Text> ();

		if (GameObject.Find ("GlobalSettings").GetComponent<GlobalSettings> ().isDisplayFPSEnabled) {

			InvokeRepeating ("SetFPS", 0.0f, 1.0f);

		} else {

			fpsString.text = "";

		}
	}
	
	// Update is called once per frame
	void Update () {

		fps = (int)(1.0f / Time.deltaTime);

	}

	void SetFPS() {

		fpsString.text = fps.ToString();

	}
}
