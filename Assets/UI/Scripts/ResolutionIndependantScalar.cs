﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ResolutionIndependantScalar : MonoBehaviour {

	public float distanceFromLeftDivisor = 2.0f;
	public float distanceFromBottomDivisor = 2.0f;

	// Use this for initialization
	void Start () {

		GetComponent<RectTransform> ().SetPositionAndRotation (new Vector3(Screen.width / distanceFromLeftDivisor, Screen.height / distanceFromBottomDivisor, 0.0f), Quaternion.identity);

	}

}
