﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AmmoDisplay : MonoBehaviour {

	private float ammo = 0;

	// Use this for initialization
	void Start () {

		ammo = GameObject.Find("ShootButtonHandler").GetComponent<Ammo>().currentAmmo;

	}

	// Update is called once per frame
	void Update () {

		ammo = GameObject.Find ("ShootButtonHandler").GetComponent<Ammo> ().currentAmmo;
		GetComponent<Slider> ().value = ammo;

	}

}
