﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonAudioScript : MonoBehaviour {

	public void PlayButtonAudio() {

		GetComponent<AudioSource> ().Play ();

	}
}
