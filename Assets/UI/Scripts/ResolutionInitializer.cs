﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResolutionInitializer : MonoBehaviour {

	// Use this for initialization
	void Start () {

		GetComponent<Dropdown> ().value = (int)GameObject.Find ("GlobalSettings").GetComponent<GlobalSettings> ().resolutionSettings;

	}
}
