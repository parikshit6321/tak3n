﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightShaftsInitializer : MonoBehaviour {

	// Use this for initialization
	void Start () {

		GetComponent<Toggle> ().isOn = GameObject.Find ("GlobalSettings").GetComponent<GlobalSettings> ().lightShaftsSettings;

	}
}
