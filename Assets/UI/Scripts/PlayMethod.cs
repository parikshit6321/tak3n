﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayMethod : MonoBehaviour {

	public Sprite loadingScreen = null;

	// Use this for initialization
	public void ChangeScene(string sceneName)
	{

		GameObject.Find ("LevelsMenu").SetActive (false);
		GameObject.Find ("background").GetComponent<UnityEngine.UI.Image>().sprite = loadingScreen;
		SceneManager.LoadScene (sceneName);

	}	
}
