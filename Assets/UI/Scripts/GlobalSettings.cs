﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class GlobalSettings : MonoBehaviour {

	public enum GraphicsSettings
	{
		OFF,
		LOW,
		MEDIUM,
		HIGH
	};

	public enum ResolutionSettings
	{
		P640,
		P720,
		P1080
	};

	public bool isVibrationEnabled = true;
	public bool isDisplayFPSEnabled = true;
	public float masterVolume = 1.0f;
	public float environmentVolume = 1.0f;
	public float gameplayVolume = 1.0f;
	public GraphicsSettings bloomSettings = GraphicsSettings.HIGH;
	public GraphicsSettings ambientOcclusionSettings = GraphicsSettings.HIGH;
	public bool lightShaftsSettings = true;
	public bool antiAliasingSettings = true;
	public ResolutionSettings resolutionSettings = ResolutionSettings.P720;

	private string readString = null;
	private string settingsFilePath = null;

	void Start () {
		
		settingsFilePath = Application.persistentDataPath + "/GlobalSettings.config";

		if (!File.Exists (settingsFilePath)) {

			FileStream createrStream = File.Create (settingsFilePath);
			createrStream.Close ();
			WriteSettingsFile ();

		} else {
			
			ReadSettingsFile ();

		}

		AudioListener.volume = masterVolume;
	}

	public void SetVibration() {

		isVibrationEnabled = GameObject.Find ("VibrationToggle").GetComponent<Toggle> ().isOn;

	}

	public void SetMasterVolume() {

		masterVolume = GameObject.Find ("MasterVolume").GetComponent<Slider> ().value;
		AudioListener.volume = masterVolume;

	}

	public void SetEnvironmentVolume() {

		environmentVolume = GameObject.Find ("EnvironmentVolume").GetComponent<Slider> ().value;

	}

	public void SetGameplayVolume() {

		gameplayVolume = GameObject.Find ("GameplayVolume").GetComponent<Slider> ().value;

	}

	public void SetBloomSettings() {

		bloomSettings = (GraphicsSettings)(GameObject.Find ("Bloom").GetComponent<Slider> ().value);

	}

	public void SetLightShaftsSettings() {

		lightShaftsSettings = GameObject.Find ("LightShafts").GetComponent<Toggle> ().isOn;

	}

	public void SetAntiAliasingSettings() {

		antiAliasingSettings = GameObject.Find ("AntiAliasing").GetComponent<Toggle> ().isOn;

	}

	public void SetAmbientOcclusionSettings() {

		ambientOcclusionSettings = (GraphicsSettings)(GameObject.Find ("AmbientOcclusion").GetComponent<Slider> ().value);

	}

	public void SetResolutionSettings() {

		resolutionSettings = (ResolutionSettings)(GameObject.Find ("Resolution").GetComponent<Dropdown> ().value);

	}

	public void SetDisplayFPS() {

		isDisplayFPSEnabled = GameObject.Find ("DisplayFPSToggle").GetComponent<Toggle> ().isOn;

	}

	public void ReadSettingsFile() {

		StreamReader reader = new StreamReader (settingsFilePath);

		readString = reader.ReadLine ().Split (' ') [1];
		isVibrationEnabled = bool.Parse (readString);

		readString = reader.ReadLine ().Split (' ') [1];
		isDisplayFPSEnabled = bool.Parse(readString);

		readString = reader.ReadLine ().Split (' ') [1];
		masterVolume = float.Parse (readString);

		readString = reader.ReadLine ().Split (' ') [1];
		environmentVolume = float.Parse (readString);

		readString = reader.ReadLine ().Split (' ') [1];
		gameplayVolume = float.Parse (readString);

		readString = reader.ReadLine ().Split (' ') [1];
		bloomSettings = ParseGraphicsSettings (readString);

		readString = reader.ReadLine ().Split (' ') [1];
		ambientOcclusionSettings = ParseGraphicsSettings (readString);

		readString = reader.ReadLine ().Split (' ') [1];
		lightShaftsSettings = bool.Parse (readString);

		readString = reader.ReadLine ().Split (' ') [1];
		antiAliasingSettings = bool.Parse (readString);

		readString = reader.ReadLine ().Split (' ') [1];
		resolutionSettings = ParseResolutionSettings (readString);

		reader.Close ();

	}

	public void WriteSettingsFile() {

		StreamWriter writer = new StreamWriter(settingsFilePath, false);
		writer.WriteLine ("VibrationEnabled " + isVibrationEnabled.ToString());
		writer.WriteLine ("DisplayFPSEnabled " + isDisplayFPSEnabled.ToString());
		writer.WriteLine ("MasterVolume " + masterVolume.ToString ());
		writer.WriteLine ("EnvironmentVolume " + environmentVolume.ToString ());
		writer.WriteLine ("GameplayVolume " + gameplayVolume.ToString ());
		writer.WriteLine ("Bloom " + bloomSettings.ToString ());
		writer.WriteLine ("AmbientOcclusion " + ambientOcclusionSettings.ToString ());
		writer.WriteLine ("LightShafts " + lightShaftsSettings.ToString ());
		writer.WriteLine ("AntiAliasing " + antiAliasingSettings.ToString ());
		writer.WriteLine ("Resolution " + resolutionSettings.ToString ());
		writer.Close();

	}

	private GraphicsSettings ParseGraphicsSettings(string inputString) {

		if (inputString.CompareTo ("OFF") == 0) {
			return GraphicsSettings.OFF;
		} else if (inputString.CompareTo ("LOW") == 0) {
			return GraphicsSettings.LOW;
		} else if (inputString.CompareTo ("MEDIUM") == 0) {
			return GraphicsSettings.MEDIUM;
		} else {
			return GraphicsSettings.HIGH;
		}

	}

	private ResolutionSettings ParseResolutionSettings(string inputString) {

		if (inputString.CompareTo ("P640") == 0) {
			return ResolutionSettings.P640;
		} else if (inputString.CompareTo ("P720") == 0) {
			return ResolutionSettings.P720;
		} else {
			return ResolutionSettings.P1080;
		}

	}
}
