﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameplayVolumeInitializer : MonoBehaviour {

	// Use this for initialization
	void Start () {

		GetComponent<Slider> ().value = GameObject.Find ("GlobalSettings").GetComponent<GlobalSettings> ().gameplayVolume;

	}
}
