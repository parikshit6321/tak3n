﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthDisplay : MonoBehaviour {

	private int health = 0;

	// Use this for initialization
	void Start () {

		health = 100;
	
	}

	// Update is called once per frame
	void Update () {

		health = GameObject.Find ("FirstPersonCharacter").GetComponent<Health> ().health;
		GetComponent<Slider> ().value = health;

	}

}
